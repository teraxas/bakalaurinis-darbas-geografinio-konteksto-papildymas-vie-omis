package lt.mesgalis.emergency.ui.maptools;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;
import lt.mesgalis.commons.geotools.KarGISFeatureFactory;
import lt.mesgalis.emergency.loader.MapLoader;
import org.geotools.data.FeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.swing.MapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.Feature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * Created by karolis on 15.5.31.
 */
public class DangerCalculatorTool extends CursorTool {
    protected Logger LOG = Logger.getLogger(this.getClass().getName());

    private static final String RES_GRID_GEOM = "element";

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final Consumer<Feature> featureConsumer;

    private Layer gridLayer;

    public DangerCalculatorTool(Map<String, Layer> layers, Supplier<Layer> gridLayerSupplier, Consumer<Feature> featureConsumer) {
        this.featureConsumer = featureConsumer;
        Optional.ofNullable(layers.get(MapLoader.PRANESIMAI)).ifPresent(p -> p.setSelected(false));
        Optional.ofNullable(layers.get(MapLoader.OBJEKTAI)).ifPresent(p -> p.setSelected(false));
        Optional.ofNullable(layers.get(MapLoader.PAVOJUS)).ifPresent(p -> p.setSelected(false));
        Optional.ofNullable(layers.get(MapLoader.PAVOJUS_LINES)).ifPresent(p -> p.setSelected(false));
//        layers.get(MapLoader.PRANESIMAI).setSelected(false);
//        layers.get(MapLoader.OBJEKTAI).setSelected(false);
        layers.get(MapLoader.TERITORIJOS).setSelected(false);
        layers.get(MapLoader.TERITORIJOS_HEX).setSelected(false);
        layers.get(MapLoader.KELIAI).setSelected(false);
        layers.get(MapLoader.PASTATAI).setSelected(false);
//        layers.get(MapLoader.PAVOJUS).setSelected(false);
//        layers.get(MapLoader.PAVOJUS_LINES).setSelected(false);
        layers.get(MapLoader.VANDUO).setSelected(false);
        layers.get(MapLoader.UPES).setSelected(false);

        gridLayer = gridLayerSupplier.get();
        gridLayer.setSelected(true);
    }

    @Override
    public void onMouseClicked(MapMouseEvent ev) {
        List<Feature> features = selectFeatures(ev);
        for (Feature feature : features) {
            LOG.info("Calculating danger on territory: " + feature.getIdentifier().getID());
            this.featureConsumer.accept(feature);
        }
    }

    private List<Feature> selectFeatures(MapMouseEvent ev) {

        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        java.awt.Point screenPos = ev.getPoint();
        Rectangle screenRect = new Rectangle(screenPos.x - 2, screenPos.y - 2, 5, 5);

        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                getMapPane().getMapContent().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(RES_GRID_GEOM), ff.literal(bbox));

        return select(gridLayer.getFeatureSource(), filter);
    }

    /**
     * Use the filter to identify the selected features
     */
    private List<Feature> select(FeatureSource featureSource, Filter filter) {

        List<Feature> selected = new ArrayList<>();
        try {
            FeatureCollection selectedFeatures = featureSource.getFeatures(filter);
            FeatureIterator iter = selectedFeatures.features();

            try {
                while (iter.hasNext()) {
                    Feature feature = iter.next();
                    selected.add(feature);
                }

            } finally {
                iter.close();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return selected;
    }
}
