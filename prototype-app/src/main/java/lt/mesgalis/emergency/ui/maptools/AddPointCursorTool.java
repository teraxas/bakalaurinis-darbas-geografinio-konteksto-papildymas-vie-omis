package lt.mesgalis.emergency.ui.maptools;

import com.vividsolutions.jts.geom.*;
import lt.mesgalis.commons.geotools.KarGISFeatureFactory;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.swing.MapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Karolis on 2015-05-10.
 */
public class AddPointCursorTool extends CursorTool {

    protected Logger LOG = Logger.getLogger(this.getClass().getName());
    private final SimpleFeatureBuilder pointFeatureBuilder;

    private final GeometryFactory geometryFactory;

    private final Consumer<SimpleFeature> pointCallbackFunction;
    private final Supplier<List> dataSupplier;

    private static Integer idSequence = 0;

    public AddPointCursorTool(MapPane mapPane, Consumer<SimpleFeature> pointCallbackFunction, SimpleFeatureType simpleFeatureType, Supplier<List> dataSupplier) {
        this.setMapPane(mapPane);
        this.pointCallbackFunction = pointCallbackFunction;
        this.dataSupplier = dataSupplier;
        pointFeatureBuilder = new SimpleFeatureBuilder(simpleFeatureType);
        geometryFactory = new GeometryFactory();
    }

    @Override
    public void onMouseClicked(MapMouseEvent ev) {
        KarGISFeatureFactory.getInstance().changeCRSIfNeeded(getMapPane().getMapContent().getCoordinateReferenceSystem());

        Point point = geometryFactory.createPoint(new Coordinate(ev.getWorldPos().getX(), ev.getWorldPos().getY()));
        pointCallbackFunction.accept(createFeature(point));
    }

    private SimpleFeature createFeature(Geometry geometry) {
        SimpleFeature newFeature = null;
        Geometry geom = geometry;
        idSequence++;
        LOG.log(Level.INFO, "Adding geometry {0}, {1}", new Object[]{idSequence, geom.getCoordinate().toString()});

        if (geom instanceof Point) {
            pointFeatureBuilder.add(geom);
            pointFeatureBuilder.add(idSequence);
            for (Object o : dataSupplier.get()) {
                pointFeatureBuilder.add(o);
            }
            newFeature = pointFeatureBuilder.buildFeature(null);
        } else {
            throw new IllegalStateException("Illegal geometry type");
        }
        return newFeature;
    }

}
