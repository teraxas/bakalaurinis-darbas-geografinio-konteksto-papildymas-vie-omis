package lt.mesgalis.emergency.ui;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import lt.mesgalis.emergency.EventTypeEnum;
import lt.mesgalis.emergency.MapUtils;
import lt.mesgalis.emergency.ObjectTypeEnum;
import lt.mesgalis.emergency.calculator.EmergencyCalculator;
import lt.mesgalis.emergency.loader.MapLoader;
import lt.mesgalis.emergency.ui.maptools.AddPointCursorTool;
import lt.mesgalis.emergency.ui.maptools.DangerCalculatorTool;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.swing.JMapFrame;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.spatial.Intersects;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main ui window wrapper.
 *
 * Created by Karolis on 2015-03-24.
 */
public class MapWindow extends JMapFrame {
    private static final Logger LOG = Logger.getLogger(MapWindow.class.getName());

    private static final String WINDOW_TITLE = "Emergency";
    private static final String RES_PRANESIMAI = "Pranešimai";
    private static final String RES_OBJEKTAI = "Objektai";
    private static final String RES_PAVOJUS = "Pavojaus zonos";
    private static final String RES_PAVOJUS_LINE = "Pavojaus linijos";
    private static final String COL_GEOM_GRID = "element";
    private static final String COL_GEOM = "the_geom";
    private static final String COL_ID = "id";

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final GeometryFactory geometryFactory = new GeometryFactory();
    private SimpleFeatureBuilder dangerZoneFeatureBuilder;

    private SimpleFeatureCollection pranesimaiCollection = FeatureCollections.newCollection();
    private SimpleFeatureCollection objektaiCollection = FeatureCollections.newCollection();
    private SimpleFeatureCollection dangerZoneCollection = FeatureCollections.newCollection();
    private SimpleFeatureCollection dangerZoneLinesCollection = FeatureCollections.newCollection();
    private Layer pranesimaiLayer = null;
    private Layer objektaiLayer = null;
    private Layer dangerZoneLayer = null;
    private Layer dangerZoneLinesLayer = null;

    private List objektaiData = new ArrayList();

    private List pranesimaiData = new ArrayList();
    private Integer dangerZoneIdSeq = 0;
    private Map<String, Layer> layers;

    private boolean hexMode = false;

    public MapWindow() {
        initFrame();
        initTools();
        this.setMapContent(new MapContent());
        this.getMapContent().setTitle(WINDOW_TITLE);
        objektaiData.add(ObjectTypeEnum.SAUGUS);
        pranesimaiData.add(EventTypeEnum.SPROGIMAS);
        dangerZoneFeatureBuilder = new SimpleFeatureBuilder(createDangerZoneFeatureType());
    }

    private void initFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        enableLayerTable(true);
        enableStatusBar(true);
        enableToolBar(true);
        enableInputMethods(true);
        setSize(1000, 800);
        setTitle(WINDOW_TITLE);
        setLocationRelativeTo(null);
        setVisible(true);

        enableTool(JMapFrame.Tool.values());
        initComponents();
    }

    private void initTools() {
        JButton button;

        button = new JButton();
        button.setText("Pranešimas");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMapPane().setCursorTool(new AddPointCursorTool(getMapPane(), getPranesimaiCollectionCallback(), createPranesimasFeatureType(), getDataForPranesimaiSupplier()));
            }
        });
        getToolBar().add(button);


        button = new JButton();
        button.setText("Objektas");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMapPane().setCursorTool(new AddPointCursorTool(getMapPane(), getObjektaiCollectionCallback(), createObjektasFeatureType(), getDataForObjektaiSupplier()));
            }
        });
        getToolBar().add(button);


        button = new JButton();
        button.setText("Pavojaus tikrinimas");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMapPane().setCursorTool(new DangerCalculatorTool(layers, getTeritorijosLayerSupplier(), getSelectedTerritoryConsumer()));
            }
        });
        getToolBar().add(button);
    }

    public void addLayer(Layer layer) {
        this.getMapPane().getMapContent().addLayer(layer);
    }

    private Consumer<SimpleFeature> getPranesimaiCollectionCallback() {
        return new Consumer<SimpleFeature>() {
            public void accept(SimpleFeature point) {
                pranesimaiCollection.add(point);

                if (pranesimaiLayer != null) {
                    processWithPolygons(point);
                    return;
                }

                // Layer is not created
                LOG.info("Creating Pranesimai layer");
                pranesimaiLayer = new FeatureLayer(pranesimaiCollection, SLD.createPointStyle("Cross", Color.RED, Color.RED, 1, 10));
                pranesimaiLayer.setTitle(RES_PRANESIMAI);
                addLayer(pranesimaiLayer);
                layers.put(MapLoader.PRANESIMAI, pranesimaiLayer);
            }
        };
    }

    private Consumer<SimpleFeature> getObjektaiCollectionCallback() {
        return new Consumer<SimpleFeature>() {
            public void accept(SimpleFeature point) {
                objektaiCollection.add(point);

                if (objektaiLayer != null) {
                    return;
                }

                // Layer is not created
                LOG.info("Creating Objektai layer");
                objektaiLayer = new FeatureLayer(objektaiCollection, SLD.createPointStyle("Circle", Color.BLUE, Color.BLUE,1,10));
                objektaiLayer.setTitle(RES_OBJEKTAI);
                addLayer(objektaiLayer);
                layers.put(MapLoader.OBJEKTAI, objektaiLayer);
            }
        };
    }

    private void addDangerZone(SimpleFeature polygon) {
        dangerZoneCollection.add(polygon);

        if (dangerZoneLayer != null) {
            return;
        }

        LOG.info("Creating dangerZone layer");
        dangerZoneLayer = new FeatureLayer(dangerZoneCollection, SLD.createPolygonStyle(Color.BLACK, Color.RED, 0.3f));
        dangerZoneLayer.setTitle(RES_PAVOJUS);
        addLayer(dangerZoneLayer);
        layers.put(MapLoader.PAVOJUS, dangerZoneLayer);
    }

    private void addDangerZoneLine(SimpleFeature line) {
        dangerZoneLinesCollection.add(line);

        if (dangerZoneLinesLayer != null) {
            return;
        }

        LOG.info("Creating dangerZone lines layer");
        dangerZoneLinesLayer = new FeatureLayer(dangerZoneLinesCollection, SLD.createLineStyle(Color.RED, 1));
        dangerZoneLinesLayer.setTitle(RES_PAVOJUS_LINE);
        addLayer(dangerZoneLinesLayer);
        layers.put(MapLoader.PAVOJUS_LINES, dangerZoneLinesLayer);
    }

    /**
     * process point to polygons if needed:
     * 1. Retrieve the territory
     * 2. Retrieve points in territory
     * 3. if 3 points - draw polygon
     * 4. if more - check if outside
     * 5. if outside - redraw polygon
     * @param point
     */
    private void processWithPolygons(SimpleFeature point) {
        LOG.info("Checking polygons...");
        Intersects territoryFilter = ff.intersects(ff.property(COL_GEOM_GRID), ff.literal(point.getDefaultGeometryProperty().getValue()));
        SimpleFeatureCollection teritorijaFilterResults = null;
        SimpleFeatureCollection points = null;
        try {
            teritorijaFilterResults = (SimpleFeatureCollection) getTeritorijos().getFeatureSource().getFeatures(territoryFilter);
            if (teritorijaFilterResults.size() != 1) {
                LOG.log(Level.SEVERE, "territoryFilter failed - size: " + teritorijaFilterResults.size());
                return;
            }

            points = MapUtils.intersects(teritorijaFilterResults, (SimpleFeatureCollection) layers.get(MapLoader.PRANESIMAI).getFeatureSource().getFeatures());
            LOG.info("Retrieved other points: " + points.size());
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "territoryFilter failed");
            e.printStackTrace();
            return;
        }

        if (points.size() == 3) {
            LOG.info("Creating new polygon");
            Coordinate[] coords = MapUtils.extractCoordinatesForLinestring(points);
            LinearRing linearRing = geometryFactory.createLinearRing(coords);
            Polygon polygon = geometryFactory.createPolygon(linearRing, null);
            addDangerZone(createDangerZonePolygonFeature(polygon));
//            addDangerZoneLine(createDangerZoneLineFeature(linearRing));
        } else if (points.size() > 3) {
            // TODO : redraw poly
            /*
            Ar viduje?
            Ne - rasti 2 artimiausius ta6kus poligone,
                 Prid4ti tarp jų
            Taip - do nothing
             */
        } else {
            return;
        }

    }

    private Supplier<List> getDataForObjektaiSupplier() {
        return new Supplier<List>() {
            public List get() {
                return objektaiData;
            }
        };
    }

    private Supplier<List> getDataForPranesimaiSupplier() {
        return new Supplier<List>() {
            public List get() {
                return pranesimaiData;
            }
        };
    }

    private SimpleFeatureType createPranesimasFeatureType() {
        SimpleFeatureTypeBuilder builder = createFeatureTypeBuilder(Point.class);
        builder.add("Type", EventTypeEnum.class);
        return builder.buildFeatureType();
    }

    private SimpleFeatureType createObjektasFeatureType() {
        SimpleFeatureTypeBuilder builder = createFeatureTypeBuilder(Point.class);
        builder.add("Type", ObjectTypeEnum.class);
        return builder.buildFeatureType();
    }

    private SimpleFeatureType createDangerZoneFeatureType() {
        SimpleFeatureTypeBuilder builder = createFeatureTypeBuilder(Polygon.class);
        builder.add("TerritoryId", Integer.class);
        return builder.buildFeatureType();
    }

    private SimpleFeatureTypeBuilder createFeatureTypeBuilder(Class geometryClass) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(getMapPane().getMapContent().getCoordinateReferenceSystem()); // <- Coordinate reference system

        // add attributes in order
        builder.add(COL_GEOM, geometryClass);
        builder.add(COL_ID, Integer.class);

        // build the type
        return builder;
    }

    private SimpleFeature createDangerZonePolygonFeature(Geometry geometry) {
        SimpleFeature newFeature = null;
        Geometry geom = geometry;
        dangerZoneIdSeq++;
        LOG.log(Level.INFO, "Adding dangerZoneLayer {0}, {1}", new Object[]{dangerZoneIdSeq, geom.getCoordinate().toString()});

        if (geom instanceof Polygon) {
            dangerZoneFeatureBuilder.add(geom);
            dangerZoneFeatureBuilder.add(dangerZoneIdSeq);
            newFeature = dangerZoneFeatureBuilder.buildFeature(null);
        } else {
            throw new IllegalStateException("Illegal geometry type");
        }
        return newFeature;
    }

    private SimpleFeature createDangerZoneLineFeature(Geometry geometry) {
        SimpleFeature newFeature = null;
        Geometry geom = geometry;
        LOG.log(Level.INFO, "Adding dangerZoneLayer {0}, {1}", new Object[]{dangerZoneIdSeq, geom.getCoordinate().toString()});

        if (geom instanceof LinearRing) {
            dangerZoneFeatureBuilder.add(geom);
            dangerZoneFeatureBuilder.add(dangerZoneIdSeq);
            newFeature = dangerZoneFeatureBuilder.buildFeature(null);
        } else {
            throw new IllegalStateException("Illegal geometry type");
        }
        return newFeature;
    }

    private Consumer<Feature> getSelectedTerritoryConsumer() {
        return new Consumer<Feature>() {
            @Override
            public void accept(Feature feature) {
                EmergencyCalculator ec = new EmergencyCalculator(layers, feature, getDangerResultConsumer());
                ec.start();
            }
        };
    }

    private Consumer<Integer> getDangerResultConsumer() {
        return new Consumer<Integer>() {
            @Override
            public void accept(Integer result) {
                LOG.info("Danger result: " + result);
                JOptionPane.showMessageDialog(null, "Pavojaus įvertinimas: " + result);
            }
        };
    }

    public Supplier<Layer> getTeritorijosLayerSupplier() {
        return new Supplier<Layer>() {
            @Override
            public Layer get() {
                return getTeritorijos();
            }
        };
    }

    public Layer getTeritorijos() {
        return hexMode ? layers.get(MapLoader.TERITORIJOS_HEX) : layers.get(MapLoader.TERITORIJOS);
    }

    public Map<String, Layer> getLayers() {
        return layers;
    }
    public void setLayers(Map<String, Layer> layers) {
        this.layers = layers;
    }
    public boolean isHexMode() {
        return hexMode;
    }
    public void setHexMode(boolean hexMode) {
        this.hexMode = hexMode;
    }
    public List getObjektaiData() {
        return objektaiData;
    }
    public void setObjektaiData(List objektaiData) {
        this.objektaiData = objektaiData;
    }
    public List getPranesimaiData() {
        return pranesimaiData;
    }
    public void setPranesimaiData(List pranesimaiData) {
        this.pranesimaiData = pranesimaiData;
    }
}
