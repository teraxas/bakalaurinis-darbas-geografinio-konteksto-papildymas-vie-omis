package lt.mesgalis.emergency.ui;

import lt.mesgalis.emergency.EventTypeEnum;
import lt.mesgalis.emergency.ObjectTypeEnum;
import lt.mesgalis.emergency.loader.MapLoader;

import javax.swing.*;
import java.awt.event.*;

public class Settings extends JDialog {
    private JPanel contentPane;
    private JRadioButton hexOption;
    private JRadioButton squareOption;
    private JComboBox pranesimasCombo;
    private JComboBox objektasCombo;
    private MapWindow mapWindow;

    public Settings(MapWindow mapWindow) {
        setContentPane(contentPane);
        setModal(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        this.mapWindow = mapWindow;

        pranesimasCombo.setModel(new DefaultComboBoxModel<>(EventTypeEnum.values()));
        objektasCombo.setModel(new DefaultComboBoxModel<>(ObjectTypeEnum.values()));

        ButtonGroup group = new ButtonGroup();
        group.add(hexOption);
        group.add(squareOption);

        hexOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Settings.this.mapWindow.setHexMode(true);
                mapWindow.getLayers().get(MapLoader.TERITORIJOS_HEX).setVisible(true);
                mapWindow.getLayers().get(MapLoader.TERITORIJOS).setVisible(false);
            }
        });
        squareOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Settings.this.mapWindow.setHexMode(false);
                mapWindow.getLayers().get(MapLoader.TERITORIJOS_HEX).setVisible(false);
                mapWindow.getLayers().get(MapLoader.TERITORIJOS).setVisible(true);
            }
        });

        pranesimasCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Settings.this.mapWindow.getPranesimaiData().clear();
                Settings.this.mapWindow.getPranesimaiData().add(pranesimasCombo.getSelectedItem());
            }
        });
        objektasCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Settings.this.mapWindow.getObjektaiData().clear();
                Settings.this.mapWindow.getObjektaiData().add(objektasCombo.getSelectedItem());
            }
        });
    }

    public void showDialog() {
        Settings dialog = new Settings(mapWindow);
        dialog.pack();
        dialog.setVisible(true);
    }
}
