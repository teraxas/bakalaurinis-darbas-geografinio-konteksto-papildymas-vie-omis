package lt.mesgalis.emergency;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karolis on 15.5.27.
 */
public class MapUtils {

    public static final String COL_GEOM_GRID = "element";
    public static final String COL_GEOM = "the_geom";

    /**
     * checks collection for intersecting features with other geometry.
     * DOES NOT INTERSECT
     * @param geometry
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public static SimpleFeatureCollection intersects(Geometry geometry, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iteratorIn = featuresIn.features();
        while (iteratorIn.hasNext()) {
            SimpleFeature featureIn = iteratorIn.next();
            Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

            if (!geometry.intersects(geometryIn)) {
                continue;
            }

            newFeatures.add(featureIn);
        }

        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * checks collection for intersecting features with other collection.
     * DOES NOT INTERSECT
     * @param features collection of features
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public static SimpleFeatureCollection intersects(SimpleFeatureCollection features, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();

            SimpleFeatureIterator iteratorIn = featuresIn.features();
            while (iteratorIn.hasNext()) {
                SimpleFeature featureIn = iteratorIn.next();
                Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

                if (!geometry.intersects(geometryIn)) {
                    continue;
                }

                newFeatures.add(featureIn);
            }
        }
        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    public static Coordinate[] extractCoordinatesForLinestring(SimpleFeatureCollection features) {
        Coordinate[] coords = new Coordinate[features.size() + 1];
        int i = 0;
        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            coords[i] = ((Geometry) iterator.next().getDefaultGeometry()).getCoordinate();
            i++;
        }
        coords[i] = coords[0];
        return coords;
    }
}
