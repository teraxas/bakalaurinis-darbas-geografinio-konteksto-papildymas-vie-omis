package lt.mesgalis.emergency.calculator;

import lt.mesgalis.emergency.MapUtils;
import lt.mesgalis.emergency.ObjectTypeEnum;
import lt.mesgalis.emergency.loader.MapLoader;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.map.Layer;
import org.opengis.feature.Feature;
import org.opengis.feature.type.FeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import java.io.IOException;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * Created by karolis on 15.4.1.
 */
public class EmergencyCalculator extends Thread {
    protected Logger LOG = Logger.getLogger(this.getClass().getName());

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

    private Map<String, Layer> layers;
    private Feature territoryFeature;
    private Consumer<Integer> resultCallback;

    public EmergencyCalculator(Map<String, Layer> layers, Feature territoryFeature, Consumer<Integer> resultCallback) {
        this.layers = layers;
        this.territoryFeature = territoryFeature;
        this.resultCallback = resultCallback;
    }

    @Override
    public void run() {
        Integer result = calculateResult();
        resultCallback.accept(result);
    }

    private Integer calculateResult() {
        LOG.info("Starting calculating result...");
        Filter mainFilter = ff.intersects(ff.property(MapUtils.COL_GEOM), ff.literal(territoryFeature.getDefaultGeometryProperty().getValue()));

        FeatureCollection<? extends FeatureType, ? extends Feature> upes = null;
        FeatureCollection<? extends FeatureType, ? extends Feature> keliai = null;
        FeatureCollection<? extends FeatureType, ? extends Feature> objektai = null;
        FeatureCollection<? extends FeatureType, ? extends Feature> pranesimai = null;
        try {
            LOG.info("Getting UPES...");
            upes = layers.get(MapLoader.UPES).getFeatureSource().getFeatures(mainFilter);
            LOG.info("UPES: " + upes.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            LOG.info("Getting KELIAI...");
            keliai = layers.get(MapLoader.KELIAI).getFeatureSource().getFeatures(mainFilter);
            LOG.info("KELIAI: " + keliai.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (layers.get(MapLoader.OBJEKTAI) != null) {
                LOG.info("Getting OBJEKTAI...");
                objektai = layers.get(MapLoader.OBJEKTAI).getFeatureSource().getFeatures(mainFilter);
                LOG.info("OBJEKTAI: " + objektai.size());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (layers.get(MapLoader.PRANESIMAI) != null) {
                LOG.info("Getting PRANESIMAI...");
                pranesimai = layers.get(MapLoader.PRANESIMAI).getFeatureSource().getFeatures(mainFilter);
                LOG.info("PRANESIMAI: " + pranesimai.size());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return calculateResult(upes, keliai, objektai, pranesimai);
    }

    private Integer calculateResult(FeatureCollection<? extends FeatureType, ? extends Feature> upes,
                                    FeatureCollection<? extends FeatureType, ? extends Feature> keliai,
                                    FeatureCollection<? extends FeatureType, ? extends Feature> objektai,
                                    FeatureCollection<? extends FeatureType, ? extends Feature> pranesimai) {
        Integer result = 0;

        if (keliai.size() > 0) {
            result -= 1;
        }

        if (objektai != null) {
            FeatureIterator<? extends Feature> iterator = objektai.features();
            while (iterator.hasNext()) {
                Feature f = iterator.next();
                ObjectTypeEnum type = (ObjectTypeEnum) f.getProperty("Type").getValue();
                switch (type) {
                    case SAUGUS:
                        result -= 1;
                        break;
                    case PAVOJINGAS:
                        result += 1;
                        break;
                }
            }
        }

        if (pranesimai != null) {
//            FeatureIterator<? extends Feature> iterator = pranesimai.features();
//            while (iterator.hasNext()) {
//                Feature f = iterator.next();
//                ObjectTypeEnum type = (ObjectTypeEnum) f.getProperty("Type").getValue();
//            }
            if (pranesimai.size() > 0) {
                result += 10;
            }
        }

        return result;
    }

}
