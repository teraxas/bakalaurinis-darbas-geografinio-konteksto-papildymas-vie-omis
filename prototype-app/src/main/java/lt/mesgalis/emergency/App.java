package lt.mesgalis.emergency;

import lt.mesgalis.emergency.loader.ShpMapLoader;
import lt.mesgalis.emergency.ui.MapWindow;
import lt.mesgalis.emergency.ui.Settings;

/**
 * Created by Karolis on 2015-03-24.
 */
public class App {

    public static void main(String args[]) {
        MapWindow mapWindow = new MapWindow();
        final ShpMapLoader loader = new ShpMapLoader(mapWindow);
        loader.load();

        Thread settingsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Settings settings = new Settings(mapWindow);
                settings.showDialog();
            }
        });
        settingsThread.start();
    }
}
