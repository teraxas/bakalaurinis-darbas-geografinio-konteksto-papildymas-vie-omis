package lt.mesgalis.emergency.loader;

import lt.mesgalis.commons.geotools.KarGISUtils;
import lt.mesgalis.emergency.ui.MapWindow;
import org.geotools.data.simple.SimpleFeatureSource;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by karolis on 15.3.31.
 */
public class ShpMapLoader extends MapLoader {

    Map<String, SimpleFeatureSource> datasource;

    public ShpMapLoader(MapWindow mapFrame) {
        super(mapFrame);
    }

    @Override
    protected void loadDataSources() {
        KarGISUtils utils = KarGISUtils.getInstance();
        datasource = new HashMap<String, SimpleFeatureSource>();
        try {
            datasource.put(MAP_KELIAI, utils.loadNewFS(new File(MAP_KELIAI)));
            datasource.put(MAP_UPES, utils.loadNewFS(new File(MAP_UPES)));
            datasource.put(MAP_VANDUO, utils.loadNewFS(new File(MAP_VANDUO)));
            datasource.put(MAP_PASTATAI, utils.loadNewFS(new File(MAP_PASTATAI)));
//            datasource.put(MAP_OBJEKTAI, utils.loadNewFS(new File(MAP_OBJEKTAI)));
        } catch (Exception e) {
            LOG.severe(e.getMessage());
        }
    }

    @Override
    public Map<String, SimpleFeatureSource> getLoadedDataSources() {
        return datasource;
    }
}
