package lt.mesgalis.emergency.loader;

import lt.mesgalis.emergency.ui.MapWindow;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.grid.Envelopes;
import org.geotools.grid.Grids;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by karolis on 15.4.1.
 */
public abstract class MapLoader {
    private static final Logger log = Logger.getLogger(MapLoader.class.getName());

    public static final String MAP_KELIAI = "shp/baltupiai-osm-roads.shp"; // TODO : set right path
    public static final String MAP_UPES = "shp/baltupiai-osm-waterways.shp"; // TODO : set right path
    public static final String MAP_VANDUO = "shp/baltupiai-osm-water.shp"; // TODO : set right path
    public static final String MAP_PASTATAI = "shp/baltupiai-osm-buildings.shp"; // TODO : set right path

    public static final String KELIAI = "Keliai";
    public static final String UPES = "Upės";
    public static final String PASTATAI = "Pastatai";
    public static final String VANDUO = "Vanduo";
    public static final String TERITORIJOS = "Teritorijos";
    public static final String TERITORIJOS_HEX = "Teritorijos hex";
    public static final String PRANESIMAI = "Pranešimai";
    public static final String OBJEKTAI = "Objektai";
    public static final String PAVOJUS = "Pavojinga zona";
    public static final String PAVOJUS_LINES = "Pavojus linijos";

    public static final double RES_GRID_WIDTH = 0.01;

    private Map<String, Layer> layers = new HashMap<String, Layer>();

    private MapWindow mapFrame = null;

    protected Logger LOG = Logger.getLogger(this.getClass().getName());

    public MapLoader(MapWindow mapFrame) {
        this.mapFrame = mapFrame;
    }

    public void load() {
        if (mapFrame == null) {
            throw new NullPointerException();
        }
        getMapFrame().setLayers(layers);
        Thread runnable = new Thread() {
            public void run() {
                loadDataSources();
                loadMaps(mapFrame);
                addGrid(mapFrame, RES_GRID_WIDTH);
                addGridHex(mapFrame, RES_GRID_WIDTH);
            }
        };

        runnable.start();
    }

    private void loadMaps(MapWindow mapFrame) {
        SimpleFeatureSource src = null;
        Style simpleStyle = null;

        FeatureLayer layer = null;

        log.log(Level.INFO, "Loading: {0}", MAP_KELIAI);
        src = getLoadedDataSources().get(MAP_KELIAI);
        simpleStyle = SLD.createSimpleStyle(src.getSchema(), Color.BLACK);
        layer=new FeatureLayer(src, simpleStyle, KELIAI);
        mapFrame.addLayer(layer);
        layers.put(KELIAI, layer);

        log.log(Level.INFO, "Loading: {0}", MAP_UPES);
        src = getLoadedDataSources().get(MAP_UPES);
        simpleStyle = SLD.createSimpleStyle(src.getSchema(), Color.BLUE);
        layer = new FeatureLayer(src, simpleStyle, UPES);
        mapFrame.addLayer(layer);
        layers.put(UPES, layer);

        log.log(Level.INFO, "Loading: {0}", MAP_PASTATAI);
        src = getLoadedDataSources().get(MAP_PASTATAI);
        simpleStyle = SLD.createSimpleStyle(src.getSchema(), Color.DARK_GRAY);
        layer = new FeatureLayer(src, simpleStyle, PASTATAI);
        mapFrame.addLayer(layer);
        layers.put(PASTATAI, layer);

        log.log(Level.INFO, "Loading: {0}", MAP_VANDUO);
        src = getLoadedDataSources().get(MAP_VANDUO);
        simpleStyle = SLD.createSimpleStyle(src.getSchema(), Color.BLUE);
        layer = new FeatureLayer(src, simpleStyle, VANDUO);
        mapFrame.addLayer(layer);
        layers.put(VANDUO, layer);
    }

    private void addGrid(MapWindow mapFrame, double size) {
        if (mapFrame.getMapContent().layers().isEmpty()) {
            log.log(Level.SEVERE, "Adding grid failed");
            return;
        }
        log.log(Level.INFO, "Adding grid");
        Layer layer = mapFrame.getMapContent().layers().get(0);

        ReferencedEnvelope gridBounds =
                Envelopes.expandToInclude(layer.getBounds(), size);

        SimpleFeatureSource grid = Grids.createSquareGrid(gridBounds, size);

        Style simpleStyle = SLD.createSimpleStyle(grid.getSchema(), Color.BLACK);
        FeatureLayer gridLayer = new FeatureLayer(grid, simpleStyle, TERITORIJOS);
        mapFrame.addLayer(gridLayer);
        layers.put(TERITORIJOS, gridLayer);
    }

    private void addGridHex(MapWindow mapFrame, double size) {
        if (mapFrame.getMapContent().layers().isEmpty()) {
            log.log(Level.SEVERE, "Adding grid failed");
            return;
        }
        log.log(Level.INFO, "Adding hexagonal grid");
        Layer layer = mapFrame.getMapContent().layers().get(0);

        ReferencedEnvelope gridBounds =
                Envelopes.expandToInclude(layer.getBounds(), size);
        SimpleFeatureSource grid = Grids.createHexagonalGrid(gridBounds, size);

        Style simpleStyle = SLD.createSimpleStyle(grid.getSchema(), Color.BLACK);
        FeatureLayer gridLayer = new FeatureLayer(grid, simpleStyle, TERITORIJOS_HEX);
        gridLayer.setVisible(false);
        mapFrame.addLayer(gridLayer);
        layers.put(TERITORIJOS_HEX, gridLayer);
    }

    abstract protected void loadDataSources();

    abstract public Map<String, SimpleFeatureSource> getLoadedDataSources();

    public MapWindow getMapFrame() {
        return mapFrame;
    }

    public void setMapFrame(MapWindow mapFrame) {
        this.mapFrame = mapFrame;
    }

}