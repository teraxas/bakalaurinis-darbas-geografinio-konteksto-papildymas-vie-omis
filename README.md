# Bakalaurinis darbas

Vilniaus Universiteto Matematikos ir informatikos fakulteto studento Karolio Jocevičiaus bakalaurinis darbas

Dokumentas kompiliuojamas su komanda

```bash
make target
```

Prototipas paleidžiamas naudojant run.bat arba run.sh failus.
Būtina turėti JRE8.
